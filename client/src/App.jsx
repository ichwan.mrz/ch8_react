// import logo from './logo.svg';
// import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import PlayerList from './components/PlayerList';
import PlayerForm from './components/PlayerForm';
import {useState} from 'react';

function App() {
  let defaultPlayerDAata = [
    {
        id : 1,
        username : "admin",
        email : "admin@gmail.com",
        password : "admin",
        exp : 10
    },
    {
        id : 2,
        username : "ichwan",
        email : "ichwan@gmail.com",
        password : "ichwan",
        exp : 10
    },
    {
        id : 3,
        username : "mirza",
        email : "mirza@gmail.com",
        password : "mirza",
        exp : 10
    }
]

const [Players, setPlayers] = useState(defaultPlayerDAata)
const [PlayerEdit, setPlayerEdit] = useState("")
const [Keyword, setKeyword] = useState('')

const addPlayer = (newPlayer) => {
  newPlayer.id = Math.max(...Players.map(player => player.id))+1
  // console.log(newPlayer.id);
  setPlayers(players => [...Players, newPlayer])
}

const selectEdit = (playerId) => {
 let player = Players.filter(player => player.id === playerId)
 setPlayerEdit(player[0]);
  }

const performEdit  = (player) => {
  console.log(`going to edit player with username ${player.username}`)
  const indexToEdit = Players.findIndex(item => item.id === player.id)
  console.log(`the player index tobe edit is ${indexToEdit}`);

  let copyPlayers = [...Players]
  copyPlayers[indexToEdit] = player

  setPlayers(copyPlayers)

}  

const cancelEdit = () => {
  setPlayerEdit('')
}

const filteredPlayerData = () => {
  return Players.filter(player => new RegExp(Keyword, 'g').test(player.username) ||
  new RegExp(Keyword, 'g').test(player.email) || 
  new RegExp(Keyword, 'g').test(player.exp))
}

  return (
    <div>
      <div className="container mt-5">
      
        <div className="row" >
        <div className="col-8">
          <div className="d-flex align-item-center mb-3">
          <h2>Data Player</h2>
          <input type="text" className="form-control" placeholder="type here..." 
          onChange={(e) => setKeyword(e.target.value)} value={Keyword}/>
          </div>
         
          <PlayerList players={filteredPlayerData()} selectEdit={selectEdit} />
        </div>

        <div className="col-4 border p-4" >
          <h2>Form Player</h2>
          <PlayerForm handleCreate={addPlayer} playerEdit={PlayerEdit} cancelEdit={cancelEdit} performEdit={performEdit}/>

        </div>
        </div>
      </div>
      
    </div>
  );
}

export default App;
